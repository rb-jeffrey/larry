<?php

namespace RBJeffrey\Larry;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use RBJeffrey\Larry\Services\Slack;

class SlackServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // Publish the configuration path
        $this->publishes([
            __DIR__.'/config/slack.php' => config_path('slack.php'),
        ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerAlias();
    }

    /**
     *  Register the Plastic alias.
     */
    protected function registerAlias()
    {
        AliasLoader::getInstance()->alias('Slack', Slack::class);
    }
}