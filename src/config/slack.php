<?php
return [
    /**
     * --------------------------------------------------------------------------
     * Slack Incoming Webhooks
     * --------------------------------------------------------------------------
     *
     * Slack > Browse Apps > Custom Integrations > Incoming WebHooks
     */
    'default_incoming_webhook' => env(
        'SLACK_DEFAULT_INCOMING_WEBHOOK',
        'https://hooks.slack.com/services/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' // change this to your own hook
    ),

    'default_link_names'    => env('SLACK_DEFAULT_LINK_NAMES'   , 1),
    'throw_exceptions'      => env('SLACK_THROW_EXCEPTIONS'     , true),
];
