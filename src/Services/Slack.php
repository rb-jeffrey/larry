<?php

namespace RBJeffrey\Larry\Services;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Collection;

/**
 * Class Slack
 *
 * @package RBJeffrey\Larry
 * @author  Jeffrey van der Schilden <jeffrey@retro-brothers.com>
 *
 */
class Slack
{
    /**
     * @var Collection
     */
    private $envs;

    /**
     * @var string
     */
    private $hook = '';

    /**
     * @var Collection
     */
    private $data;

    /**
     * Slack constructor.
     */
    public function __construct()
    {
        $this->envs = collect();
        $this->data = collect([
            'link_names' => config('slack.default_link_names')
        ]);
        $this->hook = config('slack.default_incoming_webhook');
    }

    /**
     * Ability to start the chain static
     *
     * @return Slack
     */
    private static function getSlack(): Slack
    {
        static $static = null;

        return $static = $static ?? (new static);
    }

    /**
     * @param string $hook
     * @return Slack
     */
    public static function hook(string $hook): Slack
    {
        $slack = self::getSlack();
        $slack->hook = $hook;

        return $slack;
    }

    /**
     * @param string $channel
     * @return Slack
     */
    public static function channel(string $channel): Slack
    {
        $slack = self::getSlack();
        $slack->data = $slack->data->merge(['channel' => $channel]);

        return $slack;
    }

    /**
     * @param string $text
     * @return Slack
     */
    public static function text(string $text): Slack
    {
        $slack = self::getSlack();
        $slack->data = $slack->data->merge(['text' => $text]);

        return $slack;
    }

    /**
     * @param bool $linkNames
     * @return Slack
     */
    public static function linkNames(bool $linkNames): Slack
    {
        $slack = self::getSlack();
        $linkNames ?
            $slack->data->forget('link_names') :
            $slack->data['link_names'] = 1;

        return $slack;
    }


    /**
     * Only send messages for specific environments
     *
     * @param array ...$env
     * @return Slack
     */
    public static function environment(...$env): Slack
    {
        $slack = self::getSlack();
        $slack->envs->merge(collect($env)->flatten());

        return $slack;
    }

    /**
     * @return array
     */
    public static function getData(): array
    {
        $slack = self::getSlack();
        return $slack->data->toArray();
    }

    /**
     * Post data to slack
     *
     * @param array|null  $data
     * @return bool
     */
    public static function post(?array $data = null): bool
    {
        $slack = self::getSlack();
        if (! $slack->envs->count() > 0 && $slack->envs->has(App::environment())) {
            return false;
        }

        $data = is_array($data) ? collect($data) : $slack->data;

        $ch = curl_init($slack->hook);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data->toJson());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // returns ok instead of echo
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data ?? '')
        ]);
        $success = $slack->handleResponse(curl_exec($ch));
        curl_close($ch);

        return $success;
    }

    /**
     * @param string $response
     * @return bool
     * @throws \Exception
     */
    private function handleResponse(string $response): bool
    {
        if ('ok' === $response) {
            return true;
        }

        if (config('slack.throw_exceptions')) {
            throw new \Exception($response);
        }

        return false;
    }
}