<?php

namespace RBJeffrey\Larry\Facades;

use Illuminate\Support\Facades\Facade;


class Slack extends Facade
{
    /**
     * Get a slack service instance
     *
     * @return Slack
     */
    protected static function getFacadeAccessor()
    {
        return static::$app['slack'];
    }
}