# LARRY #

### What the hell is this package? ###

* This package adds Slack webhook support to Laravel
* Easily send messages to slack via the Slack fasade

### Who is Larry? ###

* The author's personal Slack assistant
* A Super Smash Bros character, played by the author
* Little Jester

### What versions does each Larry version support? ###

|Larry     | Laravel     | PHP    |
|----------|-------------|--------|
|1.0       | 5.6         | 7.2    |

### How do fwhoops can I get this thing set up? ###

Add the rb-jeffrey/larry.git repository to your composer.json:
```
     "repositories": [
         {
             "type": "vcs",
             "url": "https://bitbucket.org/rb-jeffrey/larry.git"
```

Require larry in your project via composer require, or in the json:
```
     "require": {
         "rb-jeffrey/larry": "master"
```

Next run:

`composer update`

`php artisan vendor:publish`



### Where do I send complaints, chain letters and spam? ###

* [jeffrey@retro-brothers.com](mailto:jeffrey@retro-brothers.com)